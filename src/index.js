// constantes
const apiUrl = 'https://swapi.dev/api/'
const category = ['people', 'planets', 'species', 'starships', 'vehicles'];

const countMax = [82, 60, 37, 36, 39];

const pageMain = document.getElementById('main');
const pageCollection = document.getElementById('collection');
const getCardButton = document.getElementById('get-card');
const btnMain = document.getElementById('btn-main');
const btnCollection = document.getElementById('btn-collection');
const card = document.getElementById('card');

// constiables
let cards = [];
let canPlay = true; //flag
let countDownDate

// INIT application
if (localStorage.getItem('savedcards'))
{
    cards = loadCards();
    countDownDate = JSON.parse(localStorage.getItem('timer'));
    countDown();
}

console.log(cards)

// Listeners
getCardButton.addEventListener('click', getCard);
btnMain.addEventListener('click', showMainPage);
btnCollection.addEventListener('click', showCollectionPage);

// functions

function showMainPage()
{
    // @ts-ignore
    pageMain.style = 'display: block';
    // @ts-ignore
    pageCollection.style = 'display: none'
}

function showCollectionPage()
{
    // @ts-ignore
    pageMain.style = 'display: none';
    // @ts-ignore
    pageCollection.style = 'display: block';
    pageCollection.innerHTML = '';
    cards.forEach(card =>
    {
        pageCollection.innerHTML += `<div style="border: solid">${card.name}</div>`;
    });
}

function getCard()
{
    if (canPlay === false)
    {
        return;
    }

    fetchRandomCard();
}

async function fetchRandomCard()
{
    const randomCatIndex = Math.floor(Math.random() * category.length);
    const cat = category[randomCatIndex];
    const id = getRandomId(countMax[randomCatIndex]);

    const result = await fetch(`${apiUrl}${cat}/${id}`);
    const resultJSON = await result.json();

    if (resultJSON.detail === 'Not found')
    {
        // perdu
        return fetchRandomCard();
    }

    canPlay = false;
    showCard(resultJSON);
    saveCard(resultJSON);
    countDownDate = new Date().getTime() + 7200000;
    localStorage.setItem('timer', JSON.stringify(countDownDate));
    countDown();

    console.log(cat, id, resultJSON);
}

/**
 * @param {number} max
 * @returns {number}
 */
function getRandomId(max)
{
    return Math.floor(Math.random() * max + 1);
}

function showCard(data)
{
    card.innerHTML = data.name;
}

function saveCard(card)
{
    const urls = cards.map(e => e.url);

    if (!urls.includes(card.url))
    {
        cards.push(card);
        localStorage.setItem('savedcards', JSON.stringify(cards));
    }
}

function loadCards()
{
    const data = localStorage.getItem('savedcards');
    return JSON.parse(data);
}

function countDown()
{
    // Update the count down every 1 second
    const intervalId = setInterval(function ()
    {

        // Get today's date and time
        const now = new Date().getTime();

        // Find the distance between now and the count down date
        let storedDate = JSON.parse(localStorage.getItem('timer'));

        // anticheat
        if (storedDate !== countDownDate)
        {
            countDownDate = new Date().getTime() + 7200000;
            localStorage.setItem('timer', JSON.stringify(countDownDate));
            storedDate = JSON.parse(localStorage.getItem('timer'));
            console.log("tricher c'est mal");
        }

        canPlay = false;

        const difference = storedDate - now;

        // Time calculations for hours, minutes and seconds
        const hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        const minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((difference % (1000 * 60)) / 1000);

        // Display the result in the button element if time changed
        if (getCardButton.innerHTML !== `${hours}h ${minutes}mn ${seconds}s`)
        {
            getCardButton.innerHTML = `${hours}h ${minutes}mn ${seconds}s`;
        }

        // If the count down is finished, write some text
        if (difference < 0)
        {
            clearInterval(intervalId);
            canPlay = true;
            getCardButton.innerHTML = "Get Card";
        }
    }, 100);
}
