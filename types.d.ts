export type TUserConfig = {
    name: string;
    age: number;
}

type TSongConfig = {
    title: string;
}